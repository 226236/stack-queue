//plik zawierający definicje metod
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236

#include <iostream>
#include "funkcje.hh"
using namespace std;

//metody klasy stos
//---------
//konstruktor
stos::stos(int x)
{
  rozmiar=x;
  S=new int[x];
  sptr=0;
}

//destruktor
stos::~stos()
{
  delete [] S;
}

//sprawdzanie czy stos jest pusty
bool stos::empty(void)
{
  if(sptr=0)
    {
      return true;
    }
}

//zapisywanie na stos
void stos::push(int wart)
{
      S[sptr++]=wart;
      licznik++;
}

//usuwanie ze stosu
void stos::pop(void)
{
  if(licznik)
    {
      rozmiar--;
      licznik--;
      sptr--;
    }
  cout <<"Licznik= "<<licznik<<endl;
  cout <<"Sptr= "<<sptr<<endl;
    
}

//zwracanie szczytu stosu
int stos::top(void)
{
  if(sptr)
    {
      return S[sptr-1];
    }
}

//pokazywanie stosu
void stos::pokaz()
{
  int i;
  for(i=licznik;i>0;i--)
    {
      cout <<S[sptr-i]<<endl;
      //licznik--;
    }
  licznik=rozmiar;
}

//funkcja czyszczenia stosu
void stos::czysc()
{
  int i;
  for(i=0;i<=rozmiar;i++)
    {
      pop();
    }
}
