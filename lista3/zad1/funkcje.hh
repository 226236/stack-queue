//plik naglowkowy z definicjami klas i prototypami metod
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236

#ifndef FUNKCJE_HH
#define FUNKCJE_HH

class stos
{
private:
  int rozmiar; //rozmiar tablicy
  int sptr;    //wskaznik stosu
  int *S;  //tablica dynamiczna
  int licznik; //liczba elementow stosu
public:
  stos(int x); //konstruktor
  ~stos();     //destruktor
  bool empty(void);
  void push(int wart);
  void pop(void);
  int top(void);
  void pokaz(void);
  void czysc(void);
};

#endif
