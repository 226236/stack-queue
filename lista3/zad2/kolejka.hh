//plik z definicja klasy i metod kolejki implementowanej na tablicy
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236
#ifndef KOLEJKA_HH
#define KOLEJKA_HH

class queue
{
private:
  int rozmiar; //rozmiar tablicy
  int pocz; //wskaznik poczatku kolejki
  int licznik; //licznik elementow
  int *Q; //tablica dynamiczna
public:
  queue(int x);
  ~queue();
  bool empty(void);
  int front(void);
  void push(int v);
  void pop(void);
  void pokaz(void);
  void czysc(void);
};
#endif
