//plik programu glownego
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236
#include <iostream>
#include <cstdlib>
#include "kolejka.hh"
using namespace std;

int main()
{
  int i,wyb,rozmiarKol;
  cout <<"Podaj rozmiar kolejki: "<<endl;
  cin >> rozmiarKol;
  queue Q(rozmiarKol);
  while(1)
    {
      cout <<"1.Zapelnij kolejke"<<endl;
      cout <<"2.Pokaz kolejke"<<endl;
      cout <<"3.Wyczysc"<<endl;
      cout <<"4.Usun element"<<endl;
      cout <<"0.Koniec"<<endl;
      cin >>wyb;
      switch(wyb)
	{
	case 1:
	  {
	     for(i=1;i<=rozmiarKol;i++)
	       { 
		 Q.push(rand());
	       }
	     break;
	  }
	case 2:
	  {
	    Q.pokaz();
	    break;
	  }
	case 3:
	  {
	    Q.czysc();
	    int nowyroz;
	    cout <<"Oprozniles kolejke. Podaj rozmiar nowej kolejki: "<<endl;
	    cin >>nowyroz;
	    queue Q1(nowyroz);
	    Q=Q1;
	    break;
	  }
	case 4:
	  {
	    Q.pop();
	    if(Q.empty())
	      {
		int nowyroz;
		cout <<"Oprozniles kolejke. Podaj rozmiar nowej kolejki: "<<endl;
		cin >>nowyroz;
		queue Q1(nowyroz);
		Q=Q1;
	      }
	    break;
	  }
	case 0:
	  {
	    return 0;	 
	    break;
	  }
	}
    }
}
