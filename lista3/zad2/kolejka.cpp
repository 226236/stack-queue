//plik z definicjami metod klasy queue
//AUTOR: Mikolaj Zurowski
//NR INDEKSU: 226236
#include <iostream>
#include "kolejka.hh"
using namespace std;

//metody obiektu queue

//konstruktor
queue::queue(int x)
{
  rozmiar=x;
  Q=new int[x];
  pocz=licznik=0;
}

//destruktor
queue::~queue()
{
  delete [] Q;
}

//czy kolejka jest pusta
bool queue::empty(void)
{
  return !licznik;
}

//zwraca poczatek kolejki
int queue::front(void)
{
  if(licznik)
    {
      return Q[pocz];
    }
}

//pokazanie zawartosci kolejki
void queue::pokaz(void)
{
  //while(licznik<=rozmiar)
  //{
      for(int i=licznik;i>0;i--)
	{
	  cout <<Q[i]<<endl;
	  licznik--;
	}
      //}
  licznik=rozmiar;
}

//zapisuje do kolejki
void queue::push(int v)
{
  int i;
  if(licznik<rozmiar)
    {
      i=pocz + licznik++;
      if(i>=rozmiar)
	{
	  i -= rozmiar;
	}
      Q[i]=v;
    }
}

//usuwa z kolejki
void queue::pop(void)
{
  if(licznik)
    {
      rozmiar--;
      licznik--;
      pocz++;
    }
  cout <<"Licznik= "<<licznik<<endl;
  cout <<"Pocz= "<<pocz<<endl;
}

void queue::czysc(void)
{
  while(!empty())
    {
      pop();
    }
  pocz=licznik=0;
}
